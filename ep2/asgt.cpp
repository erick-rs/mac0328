#include "asgt.h"
#include <stack>
using namespace std;

class BCCComputation {
private:
  vector<bool> cut_vertices;
  vector<int> discovery_time, earliest_visited, parent, num_children;
  int time;
  int bcc;
  void set_biconnected_component(Graph &g, Vertex u, Vertex v, stack<Edge> &st);
  void compute_util(Graph &g, Vertex u, stack<Edge> &st);

public:
  void compute(Graph &g, bool fill_cutvxs, bool fill_bridges);
};

void BCCComputation::set_biconnected_component(Graph &g, Vertex u, Vertex v, stack<Edge> &st) {
  while (!(st.top().m_source == u and st.top().m_target == v)) {
    g[st.top()].bcc = bcc;
    st.pop();
  }

  g[st.top()].bcc = bcc;
  st.pop();
  bcc += 1;
}

void BCCComputation::compute_util(Graph &g, Vertex u, stack<Edge> &st) {
  time += 1;
  discovery_time[u] = time;
  earliest_visited[u] = time;

  for (const auto& edge : make_iterator_range(boost::out_edges(u, g))) {
    auto v = boost::target(edge, g);
    
    if (discovery_time[v] == -1) {
      num_children[u] += 1;
      parent[v] = u;
      st.push(edge);
      compute_util(g, v, st);

      earliest_visited[u] = min(earliest_visited[u], earliest_visited[v]);

      if (discovery_time[u] == 1 and num_children[u] > 1) {
        set_biconnected_component(g, u, v, st);
      } else if (discovery_time[u] > 1 and earliest_visited[v] >= discovery_time[u]) {
        set_biconnected_component(g, u, v, st);
        cut_vertices[u] = true;
      }
    } else if ((int) v != parent[u]) {
      earliest_visited[u] = min(earliest_visited[u], discovery_time[v]);

      if (discovery_time[v] < discovery_time[u]) {
        st.push(edge);
      }
    }
  }
}

void BCCComputation::compute(Graph &g, bool fill_cutvxs, bool fill_bridges) {
  stack<Edge> st;
  size_t V = boost::num_vertices(g);

  discovery_time.assign(V, -1);
  earliest_visited.assign(V, -1);
  parent.assign(V, -1);
  cut_vertices.assign(V, false);
  num_children.assign(V, 0);
  time = 0;
  bcc = 1;

  for (const auto& vertex : boost::make_iterator_range(boost::vertices(g))) {
    if (discovery_time[vertex] == -1) {
      compute_util(g, vertex, st);
    }

    bool is_stack_not_empty = !st.empty();

    while (!st.empty()) {
      g[st.top()].bcc = bcc;
      st.pop();
    }

    if (is_stack_not_empty) {
      bcc += 1;
    }
  }

  if (fill_cutvxs) {
    for (const auto& vertex : boost::make_iterator_range(boost::vertices(g))) {
      g[vertex].cutvertex = (this->parent[vertex] == -1)
        ? (num_children[vertex] > 1)
        : cut_vertices[vertex];
    }
  }

  if (fill_bridges) {
    for (const auto& edge : boost::make_iterator_range(boost::edges(g))) {
      g[edge].bridge = discovery_time[edge.m_source] > discovery_time[edge.m_target]
        ? false
        : earliest_visited[edge.m_target] > discovery_time[edge.m_source];
    }
  }
}

void compute_bcc(Graph &g, bool fill_cutvxs, bool fill_bridges) {
  BCCComputation().compute(g, fill_cutvxs, fill_bridges);
}
