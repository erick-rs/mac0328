/* This is the main file that the students must work on to submit; the
 * other one is arb.h
 */

#include "asgt.h"

Arb read_arb(std::istream& in)
{
  std::vector<std::pair<int, int>> arcs;
  size_t num_vertices;

  in >> num_vertices;

  for (int i = num_vertices - 1; i > 0; i--) {
    int u, v;
    in >> u >> v;
    arcs.push_back(std::make_pair(--u, --v));
  }

  return Arb(arcs.begin(), arcs.end(), num_vertices);
}

void preprocess_helper(Arb &arb, const Vertex& u, std::vector<int>& enter_at, std::vector<int>& exit_at, int & count)
{
  enter_at[u] = count++;

  for (const auto& arc : boost::make_iterator_range(boost::out_edges(u, arb))) {
    const Vertex& v = boost::target(arc, arb);
    preprocess_helper(arb, v, enter_at, exit_at, count);
  }

  exit_at[u] = count++;
}

HeadStart preprocess (Arb &arb, const Vertex& root)
{
  size_t num_vertices = boost::num_vertices(arb);
  std::vector<int> enter_at(num_vertices, 0);
  std::vector<int> exit_at(num_vertices, 0);
  int count = 0;

  preprocess_helper(arb, root, enter_at, exit_at, count);

  return HeadStart(enter_at, exit_at);
}

bool is_ancestor (const Vertex& u, const Vertex& v, const HeadStart& data)
{
  return data.enter_at[u] <= data.enter_at[v] and data.exit_at[v] <= data.exit_at[u];
}
