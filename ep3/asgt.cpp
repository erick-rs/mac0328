#include "asgt.h"

#include <utility>              // for std::get
#include <tuple>
#include <vector>

#define BOOST_ALLOW_DEPRECATED_HEADERS // silence warnings
#include <boost/graph/adjacency_list.hpp>
#include <boost/optional.hpp>
#include <boost/range/iterator_range.hpp>

#include "cycle.h"
#include "digraph.h"
#include "potential.h"

using boost::add_edge;
using boost::add_vertex;
using boost::remove_vertex;
using boost::num_vertices;
using boost::out_edges;
using boost::edges;
using boost::edge;
using boost::vertices;
using boost::source;
using boost::target;
using boost::make_iterator_range;
using std::vector;
using std::reverse;

Digraph build_digraph(const Digraph& market)
{
  Digraph digraph(num_vertices(market));

  for (const auto& arc : make_iterator_range(edges(market))) {
    auto u = source(arc, market);
    auto v = target(arc, market);
    auto new_arc = add_edge(u, v, digraph).first;
    digraph[new_arc].cost = -log(market[arc].cost);
  }

  return digraph;
}

std::tuple<bool,
           boost::optional<NegativeCycle>,
           boost::optional<FeasiblePotential>>
has_negative_cycle(Digraph& digraph)
{
  auto all_vertices = make_iterator_range(vertices(digraph));
  Vertex source_vertex = add_vertex(digraph);
  size_t V = num_vertices(digraph);
  vector<int> predecessor(V, -1);
  vector<double> distance(V, INFINITY);

  for (const auto& vertex : all_vertices) {
    auto arc = add_edge(source_vertex, vertex, digraph).first;
    digraph[arc].cost = 0.0;
  }

  distance[source_vertex] = 0.0;

  for (size_t i = 0; i < V; i++) {
    for (const auto& arc : make_iterator_range(edges(digraph))) {
      auto u = source(arc, digraph);
      auto v = target(arc, digraph);
      double cost = digraph[arc].cost;

      if (distance[u] != INFINITY and distance[u] + cost < distance[v]) {
        distance[v] = distance[u] + cost;
        predecessor[v] = u;
      }
    }
  }

  for (const auto& arc : make_iterator_range(edges(digraph))) {
    auto u = source(arc, digraph);
    auto v = target(arc, digraph);
    double cost = digraph[arc].cost;

    if (distance[u] != INFINITY and distance[u] + cost < distance[v]) {
      vector<Vertex> negcycle;
      vector<bool> visited(V, false);
      Vertex current, parent;

      visited[u] = true;
      parent = predecessor[u];

      while (!visited[parent]) {
        visited[parent] = true;
        parent = predecessor[parent];
      }

      current = parent;
      negcycle.push_back(current);
      current = predecessor[current];

      while (current != parent) {
        negcycle.push_back(current);
        current = predecessor[current];
      }

      reverse(negcycle.begin(), negcycle.end());

      Walk walk(digraph, current);

      for (size_t i = 0; i < negcycle.size(); i++) {
        walk.extend(edge(current, negcycle[i], digraph).first);
        current = negcycle[i];
      }

      return {true, NegativeCycle(walk), boost::none}; 
    }
  }

  remove_vertex(source_vertex, digraph);
  vector<double> potential;

  for (const auto& vertex : make_iterator_range(vertices(digraph)))
    potential.push_back(distance[vertex]);

  return {false, boost::none, FeasiblePotential(digraph, potential)};
}

Loophole build_loophole(const NegativeCycle& negcycle,
                        const Digraph& aux_digraph,
                        const Digraph& market)
{
  auto cycle = negcycle.get();
  Walk walk(market, source(cycle.front(), market));

  for (const auto& arc : cycle) {
    auto u = source(arc, aux_digraph);
    auto v = target(arc, aux_digraph);
    auto new_arc = edge(u, v, market).first;
    walk.extend(new_arc);
  }

  return Loophole(walk);
}

FeasibleMultiplier build_feasmult(const FeasiblePotential& feaspot,
                                  const Digraph& aux_digraph,
                                  const Digraph& market)
{
  vector<double> z;

  for (double value : feaspot.potential()) {
    z.push_back(1 / exp(value));
  }

  return FeasibleMultiplier(market, z);
}
