#include <iostream>
#include <stack>
#define BOOST_ALLOW_DEPRECATED_HEADERS // silence warnings
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/range/iterator.hpp>
#include <boost/range/iterator_range.hpp> // for boost::make_iterator_range

typedef boost::adjacency_list<boost::vecS,
                              boost::vecS,
                              boost::directedS> Digraph;
typedef boost::graph_traits<Digraph>::vertex_descriptor Vertex;

Vertex get_vertex_from_literal(int u, int num_variables) {
  return u < 0 ? abs(u) - 1 + num_variables : u - 1;
}

int get_literal_from_vertex(Vertex v, size_t num_variables) {
  return v >= num_variables ? -(v - num_variables + 1) : v + 1;
}

void build_digraphs_from_cnf_formula(Digraph& digraph, Digraph& inverted_graph, std::istream& in, int num_clauses) {
  int num_variables = boost::num_vertices(digraph) / 2;

  while (num_clauses > 0) {
    int u, v;
    in >> u >> v;

    boost::add_edge(
      get_vertex_from_literal(-u, num_variables),
      get_vertex_from_literal(v, num_variables),
      digraph
    );
    boost::add_edge(
      get_vertex_from_literal(-v, num_variables),
      get_vertex_from_literal(u, num_variables),
      digraph
    );

    boost::add_edge(
      get_vertex_from_literal(v, num_variables),
      get_vertex_from_literal(-u, num_variables),
      inverted_graph
    );
    boost::add_edge(
      get_vertex_from_literal(u, num_variables),
      get_vertex_from_literal(-v, num_variables),
      inverted_graph
    );

    num_clauses -= 1;
  }
}

void traverse_digraph(Digraph& digraph) {
  int num_variables = boost::num_vertices(digraph) / 2;
  std::cout << boost::num_vertices(digraph) << " " << boost::num_edges(digraph) << std::endl;

  for (const auto& vertex : boost::make_iterator_range(boost::vertices(digraph))) {
    for (const auto& arc : make_iterator_range(boost::out_edges(vertex, digraph))) {
      std::cout << get_literal_from_vertex(vertex, num_variables) << " ";
      std::cout << get_literal_from_vertex(boost::target(arc, digraph), num_variables) << std::endl;
    }
  }
}

void topological_sort_dfs(Digraph& digraph, const Vertex& u, std::vector<bool>& visited, std::stack<Vertex>& stack) {
  visited[u] = true;

  for (const auto& arc : make_iterator_range(boost::out_edges(u, digraph))) {
    const Vertex& v = boost::target(arc, digraph);
    if (!visited[v]) {
      topological_sort_dfs(digraph, v, visited, stack);
    }
  }

  stack.push(u);
}

void count_components_dfs(Digraph& t_digraph, const Vertex& u, int value, std::vector<int>& components) {
  components[u] = value;

  for (const auto& arc : make_iterator_range(boost::out_edges(u, t_digraph))) {
    const Vertex& v = boost::target(arc, t_digraph);
    if (components[v] == -1) {
      count_components_dfs(t_digraph, v, value, components);
    }
  }
}

void reach_path_from(Digraph& digraph, const Vertex& u, std::vector<bool>& visited, std::vector<Vertex>& predecessor) {
  visited[u] = true;

  for (const auto& arc : make_iterator_range(boost::out_edges(u, digraph))) {
    const Vertex& v = boost::target(arc, digraph);
    if (!visited[v]) {
      predecessor[v] = u;
      reach_path_from(digraph, v, visited, predecessor);
    }
  }
}

void print_path(Digraph& digraph, const Vertex& source, const Vertex& target) {
  size_t num_vertices = boost::num_vertices(digraph);
  size_t num_variables = num_vertices / 2;
  std::vector<bool> visited(num_vertices, false);
  std::vector<Vertex> predecessor(num_vertices, 0);
  std::vector<Vertex> path;

  reach_path_from(digraph, source, visited, predecessor);

  Vertex end = target;

  while (end != source) {
    path.push_back(end);
    end = predecessor[end];
  }

  path.push_back(end);
  std::reverse(path.begin(), path.end());

  std::cout << path.size() - 1 << " ";
  for (size_t i = 0; i < path.size(); i++) {
    const Vertex& v = path[i];
    std::cout << get_literal_from_vertex(v, num_variables);

    if (i < path.size() - 1) std::cout << " ";
  }

  std::cout << std::endl;
}

void go_to_no_assignment_possible(Digraph& digraph, const Vertex& vertex) {
  size_t num_vertices = boost::num_vertices(digraph);
  size_t num_variables = num_vertices / 2;
  std::vector<bool> visited(num_vertices, false);
  std::vector<Vertex> predecessor(num_vertices, 0);
  std::vector<Vertex> path;

  std::cout << "NO" << std::endl;
  std::cout << get_literal_from_vertex(vertex, num_variables) << std::endl;

  print_path(digraph, vertex, vertex + num_variables);
  print_path(digraph, vertex + num_variables, vertex);
}

void go_to_truth_assignment(std::vector<bool>& assignment) {
  std::cout << "YES" << std::endl;

  for (size_t i = 0; i < assignment.size(); i++) {
    if (assignment[i]) {
      std::cout << "1";
    } else {
      std::cout << "0";
    }

    if (i < assignment.size() - 1) std::cout << " ";
  }

  std::cout << std::endl;
}

std::vector<int> find_strong_components(Digraph& digraph, Digraph& inverted_graph) {
  size_t num_vertices = boost::num_vertices(digraph);
  std::stack<Vertex> stack;
  std::vector<bool> visited(num_vertices, false);
  std::vector<int> components(num_vertices, -1);

  for (const auto& vertex : boost::make_iterator_range(boost::vertices(digraph))) {
    if (!visited[vertex]) {
      topological_sort_dfs(digraph, vertex, visited, stack);
    }
  }

  int num_components = 0;
  while (!stack.empty()) {
    const Vertex& v = stack.top();
    stack.pop();

    if (components[v] == -1) {
      count_components_dfs(inverted_graph, v, num_components, components);
      num_components += 1;
    }
  }

  return components;
}

void print_strong_component_for_each_literal(std::vector<int>& components) {
  for (size_t i = 0; i < components.size(); i++) {
      std::cout << components[i] + 1;

      if (i < components.size() - 1) std::cout << " ";
    }

    std::cout << std::endl;
}

void decide_answer(Digraph& digraph, std::vector<int>& components) {
  size_t num_variables = boost::num_vertices(digraph) / 2;
  std::vector<bool> assignment(num_variables, false);

  for (size_t i = 0; i < num_variables; i++) {
    if (components[i] == components[i + num_variables]) {
      go_to_no_assignment_possible(digraph, i);
      return;
    }

    assignment[i] = components[i] > components[i + num_variables];
  }
  
  go_to_truth_assignment(assignment);
}

int main(int argc, char** argv) {
  size_t debugging_level, num_variables, num_clauses;
  std::cin >> debugging_level >> num_variables >> num_clauses;

  Digraph digraph(num_variables * 2);
  Digraph inverted_graph(num_variables * 2);

  build_digraphs_from_cnf_formula(digraph, inverted_graph, std::cin, num_clauses);

  if (debugging_level == 2) {
    traverse_digraph(digraph);
    return 0;
  }

  std::vector<int> components = find_strong_components(digraph, inverted_graph);

  if (debugging_level == 1) {
    print_strong_component_for_each_literal(components);
  } else {
    decide_answer(digraph, components);
  }

  return 0;
}
